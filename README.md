# README

## Introduction

This repository implements a simple game of Checkers/Draughts for two players.

The rules are as follows:

1. White begins
2. The player that captured the most stones wins
3. Backward moves are not allowed
4. Capturing is mandatory (no double captures)
5. The game ends when a player has no moves


## How to play
Open index.html in any browser.

### Dependencies
The game is implemented using Vue.js and thus requires javascript
to run.
