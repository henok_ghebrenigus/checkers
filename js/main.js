/* main.js
 *
 * Author: Henok Ghebrenigus
 *
 * Date: November 2021
 *
 * This file implements a simple game of Checkers/Draughts using Vue.js.
 *
 * Rules:
 *
 * 1. White begins
 * 2. The player that captured the most stones wins
 * 3. Backward moves are not allowed
 * 4. Capturing is mandatory (no double captures)
 * 5. The game ends when a player has no moves
 *
 *
 */
'use-strict';

/**********************************************************************
 * 
* Constants 
* 
*********************************************************************/
let INITIAL_BOARD_STATE = [[0, 1, 0, 1, 0, 1, 0, 1],
                           [1, 0, 1, 0, 1, 0, 1, 0],
                           [0, 1, 0, 1, 0, 1, 0, 1],
                           [0, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0],
                           [2, 0, 2, 0, 2, 0, 2, 0],
                           [0, 2, 0, 2, 0, 2, 0, 2],
                           [2, 0, 2, 0, 2, 0, 2, 0]];

let GAME_STATES = {TURN_WHITE: 0,
                   ACTIVE_WHITE: 1,
                   TURN_BLACK: 2,
                   ACTIVE_BLACK: 3,
                   GAME_OVER: 4}

let BOARD_STATES = {EMPTY: 0, 
                    WHITE_STONE_STATE: 1,
                    BLACK_STONE_STATE: 2,
                    ACTIVE_SQUARE_STATE: 3,
                    ACTIVE_BLACK_STONE_STATE: 4,
                    ACTIVE_WHITE_STONE_STATE: 5}

                    let SQUARES_PER_ROW = 8;

var app = new Vue({
    el: "#app",
    data:  {
        range: [0, 1, 2, 3, 4, 5, 6, 7], // for template loop
        show_board: true,
        game_started: false,
        white_score: 0,
        black_score: 0,
        capture_alert: false,
        width: 0,
        active_index: {x: -1, y: -1},
        game_state: GAME_STATES.TURN_WHITE,
        board_state: INITIAL_BOARD_STATE,
    },
    computed: {
        // Return board height/width
        square_width() {
            return this.width / SQUARES_PER_ROW;
        }
    },
    // Make sure the board is correctly sized.
    mounted: function() {
        window.onresize = this.get_width;
        window.onload = this.get_width;
    },
    methods: {
        // Ensure board is square.
        get_width() {
            this.width = document.getElementById("board").clientHeight;
            return this.width;
        },

        /**********************************************************************
         * 
         * Convenience functions 
         * 
         *********************************************************************/
        // Get screen coordinates of the board
        get_board_offsets() {
            let rect = document.getElementById("board").getBoundingClientRect();
            return {xoff: rect.x, yoff: rect.y}
        },
        // Convert an index into screen coordinates
        index_to_coord(x_index, y_index) {
            let offset = this.get_board_offsets();
            let x_coord = (this.width * (x_index + 0.5) / SQUARES_PER_ROW);
            let y_coord = (this.width * (y_index + 0.5) / SQUARES_PER_ROW);
            return {x: x_coord, y: y_coord};
        },
        // Convert screen coordinates into board indices.
        coord_to_index(x, y) {
            let offset = this.get_board_offsets();
            let x_index = Math.floor(SQUARES_PER_ROW * (x - offset.xoff) / this.width);
            let y_index = Math.floor(SQUARES_PER_ROW * (y - offset.yoff) / this.width); 
            return {x: x_index, y: y_index};
        },
        // Return true if x, y are valid board indices, false otherwise.
        is_valid(x, y=0) {
            return (0 <= x && x < SQUARES_PER_ROW) && (0 <= y && y < SQUARES_PER_ROW);
        },

        /**********************************************************************
         * 
         * Query board state 
         * 
         *********************************************************************/
        is_state(x_index, y_index, state) {
            if (!this.is_valid(x_index, y_index)) return;
            return this.board_state[y_index][x_index] == state;
        },
        is_white_square(x_index, y_index) {
            let is_even = (x_index - 1) % 2 == 0 && (y_index - 1) % 2 == 0; 
            let is_odd = (x_index - 1) % 2 != 0 && (y_index - 1) % 2 != 0; 
            return is_even || is_odd;
        },
        is_active_black_stone(x_index, y_index) {
            return this.is_state(x_index, y_index, BOARD_STATES.ACTIVE_BLACK_STONE_STATE);
        },
        is_active_white_stone(x_index, y_index) {
            return this.is_state(x_index, y_index, BOARD_STATES.ACTIVE_WHITE_STONE_STATE);
        },
        is_active_square(x_index, y_index) {
            return this.is_state(x_index, y_index, BOARD_STATES.ACTIVE_SQUARE_STATE);
        },
        is_active_stone(x_index, y_index) {
            return this.is_active_black_stone(x_index, y_index) || 
                this.is_active_white_stone(x_index, y_index);
        },
        is_white_stone(x_index, y_index) {
            return this.is_state(x_index, y_index, BOARD_STATES.WHITE_STONE_STATE) ||
                this.is_active_white_stone(x_index, y_index);
        },
        is_black_stone(x_index, y_index) {
            return this.is_state(x_index, y_index, BOARD_STATES.BLACK_STONE_STATE) ||
                this.is_active_black_stone(x_index, y_index);
        },
        is_stone(x_index, y_index) {
            return this.is_white_stone(x_index, y_index) || this.is_black_stone(x_index, y_index);
        },
        is_captureable_square(x_index, y_index) {
            return this.is_state(x_index, y_index, BOARD_STATES.EMPTY) || 
                this.is_state(x_index, y_index, BOARD_STATES.ACTIVE_SQUARE_STATE);
        },
        // Return true if current player can capture a stone, false otherwise
        is_capture_possible() {

            // Set variables depending on active player.
            var is_own_color, is_other_color, y_next, y_next_next;
            if (this.game_state == GAME_STATES.ACTIVE_WHITE) {

                is_own_color = this.is_white_stone;
                is_other_color = this.is_black_stone;
                y_next = (y) => y + 1; // Return next row index
                y_next_next = (y) => y + 2;  // Return second to next row index

            } else if (this.game_state == GAME_STATES.ACTIVE_BLACK) {

                is_own_color = this.is_black_stone;
                is_other_color = this.is_white_stone;

                y_next = (y) => y - 1; 
                y_next_next = (y) => y - 2; 

            } else {
                return false;
            }

            // For every position on the board.
            for (var x = 0; x < SQUARES_PER_ROW; x++) {
                for (var y = 0; y < SQUARES_PER_ROW; y++) {

                    if (!is_own_color(x, y)) continue;

                    // Check left
                    if (is_other_color(x - 1, y_next(y))) {

                        if (this.is_captureable_square(x - 2, y_next_next(y))) {
                            return true;
                        }

                    // Check right 
                    } else if (is_other_color(x + 1, y_next(y))) {

                        if (this.is_captureable_square(x + 2, y_next_next(y))) {
                            return true;
                        }
                    }
                }
            }

            return false;
        },
        // Return true if the this move to x_index y_index is a capture,
        // false otherwise
        is_capture_move(x_index, y_index) {

            var x = this.active_index.x;
            var y = this.active_index.y;

            if (this.game_state == GAME_STATES.ACTIVE_WHITE) {

                if (x_index == x + 2 && y_index == y + 2) {
                    return true;
                } else if (x_index == x - 2 && y_index == y + 2) {
                    return true;
                }

            } else  if (this.game_state == GAME_STATES.ACTIVE_BLACK) {

                if (x_index == x + 2 && y_index == y - 2) {
                    return true;
                } else if (x_index == x - 2 && y_index == y - 2) {
                    return true;
                }
            }
            
            return false;
        },
        
        /**********************************************************************
         * 
         * Query game state 
         * 
         *********************************************************************/
        is_turn_white() {
            return this.game_state == GAME_STATES.TURN_WHITE ||
                    this.game_state == GAME_STATES.ACTIVE_WHITE;
        },
        is_turn_black() {
            return this.game_state == GAME_STATES.TURN_BLACK ||
                    this.game_state == GAME_STATES.ACTIVE_BLACK;
        },
        game_is_over() {
            return this.game_state == GAME_STATES.GAME_OVER;
        },
        // Return true if any player has no more moves, false otherwise.
        is_game_end() {

            var y_next = (y) => { return y + 1; }, y_next_next;
            var is_own_color;
            if (this.is_turn_white()) {
                is_own_color = this.is_white_stone;
                y_next = (y) => { return y + 1; };
                y_next_next = (y) => { return y + 2; };
            } else if (this.is_turn_black()) {
                is_own_color = this.is_black_stone;
                y_next = (y) => { return y - 1; };
                y_next_next = (y) => { return y - 2; };
            } else {
                return false;
            }
            for (var x = 0; x < SQUARES_PER_ROW; x++) {
                for (var y = 0; y < SQUARES_PER_ROW; y++) {
                    if (is_own_color(x, y)) {
                        if (this.is_captureable_square(x - 1, y_next(y))) return false;
                        if (this.is_captureable_square(x + 1, y_next(y))) return false;
                        if (this.is_captureable_square(x - 2, y_next_next(y))) return false;
                        if (this.is_captureable_square(x + 2, y_next_next(y))) return false;
                    } 
                }
            }

            return true;
        },

        /**********************************************************************
        * 
        * Update board state
        * 
         *********************************************************************/
        set_board_state(x_index, y_index, state) {
            if (!this.is_valid(x_index, y_index)) return;
            this.$set(this.board_state[y_index], x_index, state);
        },
        set_active_white_stone(x_index, y_index) {
            this.set_board_state(x_index, y_index, BOARD_STATES.ACTIVE_WHITE_STONE_STATE);
        }, 
        set_active_black_stone(x_index, y_index) {
            this.set_board_state(x_index, y_index, BOARD_STATES.ACTIVE_BLACK_STONE_STATE);
        },
        set_active_stone(x_index, y_index) {
            if (this.is_white_stone(x_index, y_index)) {
                this.set_active_white_stone(x_index, y_index);
            } else if (this.is_black_stone(x_index, y_index)) {
                this.set_active_black_stone(x_index, y_index);
            }
        },
        set_inactive_white(x_index, y_index) {
            if (!this.is_valid(x_index, y_index)) return;
            this.set_board_state(x_index, y_index, BOARD_STATES.WHITE_STONE_STATE);
        }, 
        set_inactive_black(x_index, y_index) {
            this.set_board_state(x_index, y_index, BOARD_STATES.BLACK_STONE_STATE);
        },
        set_inactive_stone(x_index, y_index) {
            if (this.is_white_stone(x_index, y_index)) {
                this.set_inactive_white(x_index, y_index);
            } else if (this.is_active_black_stone(x_index, y_index)) {
                this.set_inactive_black(x_index, y_index);
            }
        },
        // Colour squares to show where a player can move. 
        set_active_squares() {
            
            var x = this.active_index.x;
            var y = this.active_index.y;

            // Helper functions
            let set_active = (x, y) => { 
                this.set_board_state(x, y, BOARD_STATES.ACTIVE_SQUARE_STATE)
            };

            var y_next, y_next_next, is_other_color; 
            if (this.game_state == GAME_STATES.ACTIVE_WHITE) {
              
                y_next = y + 1;
                y_next_next = y + 2;
                is_other_color = this.is_black_stone;

            } else if (this.game_state == GAME_STATES.ACTIVE_BLACK) {
               
                y_next = y - 1;
                y_next_next = y - 2;
                is_other_color = this.is_white_stone;
            
            } else {
                return;
            }

            // Look at left diagonal position.
            // If the position is on the board.
            if (this.is_valid(x - 1) && this.is_valid(y_next)) {

                // If there is no stone on that position.
                if (!this.is_stone(x - 1, y_next)) {
                    
                    set_active(x - 1, y_next);

                // Otherwise check if capture is possible
                }  else if (is_other_color(x - 1, y_next) &&
                            !this.is_stone(x - 2, y_next_next)) {
                    
                    set_active(x - 2, y_next_next); // Mark capture possibility

                }
            }

            // Look at right diagonal position
            if (this.is_valid(x + 1) && this.is_valid(y_next)) {

                // If there is no stone on that position.
                if (!this.is_stone(x + 1, y_next)) {
                
                    set_active(x + 1, y_next);
                
                }  else {
                
                    // Otherwise check if capture is possible
                    if (is_other_color(x + 1, y_next) &&
                        !this.is_stone(x + 2, y_next_next)) {
                
                            set_active(x + 2, y_next_next); // Mark capture possibility
                
                    }

                }
            }

        },
        // Remove all coloured squares
        set_inactive_squares() {
            for (var x = 0; x < SQUARES_PER_ROW; x++) {
                for (var y = 0; y < SQUARES_PER_ROW; y++) {
                    if (this.is_state(x, y, BOARD_STATES.ACTIVE_SQUARE_STATE)) {
                        this.set_board_state(x, y, BOARD_STATES.EMPTY);
                    }
                }
            }
        },
        
        // Update board state after a capture
        process_capture(x_index, y_index) {
    
            var x = this.active_index.x;
            var y = this.active_index.y;

            if (this.game_state == GAME_STATES.ACTIVE_WHITE) {
    
                if (x_index == x + 2 && y_index == y + 2) {
                    this.set_board_state(x + 1, y + 1, BOARD_STATES.EMPTY);
                } else if (x_index == x - 2 && y_index == y + 2) {
                    this.set_board_state(x - 1, y + 1, BOARD_STATES.EMPTY);
                }
    
            } else  if (this.game_state == GAME_STATES.ACTIVE_BLACK) {
    
                if (x_index == x + 2 && y_index == y - 2) {
                    this.set_board_state(x + 1, y - 1, BOARD_STATES.EMPTY);
                } else if (x_index == x - 2 && y_index == y - 2) {
                    this.set_board_state(x - 1, y - 1, BOARD_STATES.EMPTY);
                }
            }
        },

        /**********************************************************************
        * 
        * Game loop
        * 
         *********************************************************************/
        process_board_click(event) {

            this.game_started = true;
           
            let index = this.coord_to_index(event.clientX, event.clientY);
            let x_index = index.x;
            let y_index = index.y;

            // These variables determine if actions are taken for white or black
            // player
            var state_type, is_color_stone, is_active_stone,
                next_state_success, next_state_failure, next_board_state, 
                update_score;

            if (this.is_turn_white()) {
                state_type = "active";
                is_color_stone = this.is_white_stone;
                is_active_stone = this.is_active_white_stone;
                next_state_success = GAME_STATES.TURN_BLACK;
                next_state_failure = GAME_STATES.TURN_WHITE;
                next_board_state = BOARD_STATES.WHITE_STONE_STATE;
                update_score = () => { this.white_score++ };

                if (this.game_state == GAME_STATES.TURN_WHITE) {
                    state_type = "turn";
                    next_state_success = GAME_STATES.ACTIVE_WHITE;
                } 
            } else if (this.is_turn_black()) {
                state_type = "active";
                is_color_stone = this.is_black_stone;
                is_active_stone = this.is_active_black_stone;
                next_state_success = GAME_STATES.TURN_WHITE;
                next_state_failure = GAME_STATES.TURN_BLACK;
                next_board_state = BOARD_STATES.BLACK_STONE_STATE;
                update_score = () => { this.black_score++ };

                if (this.game_state == GAME_STATES.TURN_BLACK) {
                    state_type = "turn";
                    next_state_success = GAME_STATES.ACTIVE_BLACK;
                } 
            } else {
                return;
            }

            // Core game logic
            if (state_type == "turn") {
                // If player has not clicked on own stone.
                if (!is_color_stone(x_index, y_index)) return;

                // Else move to active state
                this.game_state = next_state_success;;
                this.active_index = index;
                this.set_active_stone(x_index, y_index);
                this.set_active_squares();

            } else if (state_type == "active") {

                // If player has clicked on their active stone
                if (is_active_stone(x_index, y_index)) {
               
                    this.game_state = next_state_failure;
                    this.set_inactive_stone(x_index, y_index);
                    this.set_inactive_squares();
               
                // If player has clicked on own inactive stone
                // make that stone active
                } else if (is_color_stone(x_index, y_index)) {
               
                    this.set_inactive_stone(this.active_index.x, this.active_index.y);
                    this.active_index = index;
                    this.set_active_stone(x_index, y_index);
                    this.set_inactive_squares();
                    this.set_active_squares();
               
                // If player has clicked an active square,
                // move stone to that square if possible and process capture.
                } else if (this.is_active_square(x_index, y_index)) {

                    // If a capture is made, remove opposition's stone.
                    if (this.is_capture_possible() && this.is_capture_move(x_index, y_index)) {
                        console.log("Capture made;")
                        this.process_capture(x_index, y_index);
                        update_score();
                        // console.log(score);

                    // Captures are mandatory.
                    } else if (this.is_capture_possible()) {
                        this.capture_alert = true;
                        return;
                    }

                    // Move own stone
                    this.set_board_state(this.active_index.x, this.active_index.y, BOARD_STATES.EMPTY)
                    this.set_inactive_squares();
                    this.set_board_state(x_index, y_index, next_board_state);
                    this.game_state = next_state_success;
                }
            }
            this.capture_alert = false;
            if (this.is_game_end()) {
                this.game_state = GAME_STATES.GAME_OVER;
            }
            return;
        },
    },
});







